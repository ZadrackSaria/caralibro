(function () {

	var app = angular.module("caralibro", ["ui.router"]);

	app.constant("PAGE_TAM", 3);

	app.config(function ($stateProvider, $urlRouterProvider)
	{
		$stateProvider

			// ===
			// == Public
			.state({
				name: "public",
				abstract: true,
				templateUrl: "app/views/public.html",
				controller: "PublicController",
				controllerAs: "publicCtrl"
			})
			.state({
				name: "public.register",
				url: "/register",
				templateUrl: "app/views/public.register.html",
				controller: "RegisterController",
				controllerAs: "registerCtrl"
			})

			// ===
			// == Private
			.state({
				name: "private",
				abstract: true,
				templateUrl: "app/views/private.html",
				controller: "PrivateController",
				controllerAs: "privateCtrl"
			})

			// Self profile
			.state({
				name: "private.me",
				url: "/me",
				templateUrl: "app/views/private.profile.html",
				controller: "ProfileController",
				controllerAs: "profileCtrl"
			})

			// Profiles
			.state({
				name: "private.profile",
				url: "/profile/{userId}",
				templateUrl: "app/views/private.profile.html",
				controller: "ProfileController",
				controllerAs: "profileCtrl"
			})
			.state({
				name: "private.search",
				url: "/search?query",
				templateUrl: "app/views/private.search.html",
				controller: "SearchController",
				controllerAs: "searchCtrl"
			})

			// Posts
			.state({
				name: "private.all",
				url: "/all",
				templateUrl: "app/views/private.all.html",
				controller: "AllPostsController",
				controllerAs: "allCtrl"
			})


		$urlRouterProvider.otherwise(function ($injector, $location)
		{
			// Get the $state service by the Angular $injector
			var $state = $injector.get("$state");
			var UserService = $injector.get("UserService");

			// By default, go to register
			var url = $state.href("public.register");

			// If the user is logged, go to their profile
			if (UserService.getCurrent()) url = $state.href("private.me");

			// Return URL removing first character "#" added by the $state.href() method
			return url.slice(1);
		});
	});

	app.run(function ()
	{
		Parse.initialize("qAZCpZxwVXxVsekzMCo2uE7ejJH5G6cBuF6g7uPU", "oi2RLKKiGwH8LTApU0jkZmauW6IGf3XBjlEgfXvV");
	});

})();
