(function () {

	var app = angular.module("caralibro");

	app.controller("AllPostsController", function (PostService, PAGE_TAM)
	{
		console.log("AllPostsController");
		var ctrl = this;


		// ==
		// == Properties
		// ==

		var currentPage = 0;

		ctrl.postList = [];
		ctrl.hasMorePosts = true;



		// ==
		// == Methods
		// ==

		ctrl.loadMore = function ()
		{
			currentPage++;
			loadPage(currentPage);
		}



		// ==
		// == Initialize
		// ==

		loadPage(0);



		// ==
		// == Auxiliar
		// ==

		function loadPage (page)
		{
			PostService.getAll(page).then(onGetAll, onError);
		}

		function onGetAll (postlist)
		{
			console.log("> onGetAll:");
			console.log(postlist);

			// Add the result
			ctrl.postList = ctrl.postList.concat(postlist);

			// Disable "Load more" button
			if (postlist.length < PAGE_TAM) ctrl.hasMorePosts = false;
		}

		function onError (error)
		{
			console.error("> onError:");
			console.error(error);
		}
	});

})();
