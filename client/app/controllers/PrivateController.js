(function () {

	var app = angular.module("caralibro");

	app.controller("PrivateController", function ($state, UserService)
	{
		console.log("PrivateController");
		var ctrl = this;


		// ==
		// == Properties
		// ==

		ctrl.searchQuery = "";
		ctrl.currentName = UserService.getCurrent().get("name");



		// ==
		// == Methods
		// ==

		ctrl.logout = function ()
		{
			console.log("Logout");

			UserService.logout();
			$state.go("public.register");
		}

		ctrl.search = function ()
		{
			console.log("Search");

			$state.go("private.search", { query: ctrl.searchQuery });
		}

		ctrl.refresh = function ()
		{
			console.log("Refresh");

			$state.reload();
		}
	});

})();
