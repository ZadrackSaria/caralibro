(function () {

	var app = angular.module("caralibro");

	app.controller("ProfileController", function ($state, $stateParams, UserService, PostService, PAGE_TAM)
	{
		console.log("ProfileController");
		var ctrl = this;


		// ==
		// == Properties
		// ==

		var currentPage = 0;
		var loggedUser = UserService.getCurrent();
		var userId = $state.is("private.me") ? loggedUser.id : $stateParams.userId;

		ctrl.user = {};
		ctrl.postList = [];
		ctrl.hasMorePosts = true;

		ctrl.newPostMessage = "";
		ctrl.newPostPhotoName = "";
		ctrl.newPostPhotoFile = null;

		ctrl.isMyProfile = $state.is("private.me") || ($stateParams.userId == loggedUser.id);

		ctrl.isEditingAbout = false;
		ctrl.newAbout = "";



		// ==
		// == Methods
		// ==

		ctrl.doEditAbout = function ()
		{
			if (ctrl.isEditingAbout)
			{
				UserService.updateUserInfo(ctrl.newAbout).then(function(){console.log("done")});
			}
			else
			{
				ctrl.newAbout = ctrl.user.get("about");
			}

			ctrl.isEditingAbout = !ctrl.isEditingAbout;
		}

		ctrl.doCreatePost = function ()
		{
			PostService.create(ctrl.newPostMessage, ctrl.newPostPhotoFile).then(onCreatePost, onError);
			ctrl.newPostMessage = "";
		}

		ctrl.loadMore = function ()
		{
			currentPage++;
			loadPage(currentPage);
		}

		ctrl.selectPostPhotoFile = function (file)
		{
			console.log("File selected: " + file.name);
			ctrl.newPostPhotoName = file.name;
			ctrl.newPostPhotoFile = file;
		}

		ctrl.changeProfilePhoto = function (file)
		{
			console.log("New profile photo: " + file.name);
			UserService.updateUserPhoto(file).then(onProfilePhotoChanged, onError);
		}



		// ==
		// == Initialize
		// ==

		loadUser();
		loadPage(0);



		// ==
		// == Auxiliar
		// ==

		function loadUser ()
		{
			UserService.getUser(userId).then(onGetUser, onError);
		}

		function loadPage (page)
		{
			PostService.getPostsOfUser(userId, page).then(onGetPosts, onError);
		}

		function onGetUser (user)
		{
			console.log("> onGetUser:");
			console.log(user);

			ctrl.user = user;
		}

		function onGetPosts (postlist)
		{
			console.log("> onGetPosts:");
			console.log(postlist);

			ctrl.postList = ctrl.postList.concat(postlist);

			// Disable "Load more" button
			if (postlist.length < PAGE_TAM) ctrl.hasMorePosts = false;
		}

		function onCreatePost (postCreated)
		{
			console.log("> onCreatePost:");
			console.log(postCreated);

			ctrl.postList.unshift(postCreated);
		}

		function onProfilePhotoChanged ()
		{
			loadUser();
		}

		function onError (error)
		{
			console.error("> onError:");
			console.error(error);
		}
	});

})();
