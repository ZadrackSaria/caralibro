(function () {

	var app = angular.module("caralibro");

	app.controller("PublicController", function ($state, UserService)
	{
		console.log("PublicController");
		var ctrl = this;


		// ==
		// == Properties
		// ==

		ctrl.loginUsername = "";
		ctrl.loginPassword = "";



		// ==
		// == Methods
		// ==

		ctrl.doLogin = function ()
		{
			console.log("Login");

			UserService.login(ctrl.loginUsername, ctrl.loginPassword).then(onLogin, onError);
		}

		function onLogin (user)
		{
			console.log("> onLogin:");
			console.log(user);

			$state.go("private.me");
		}

		function onError (error)
		{
			console.error("> onError:");
			console.error(error);
		}
	});

})();
