(function () {

	var app = angular.module("caralibro");

	app.controller("RegisterController", function (UserService, $state)
	{
		console.log("RegisterController");
		var ctrl = this;


		// ==
		// == Properties
		// ==

		ctrl.registerName = "";
		ctrl.registerSurnames = "";
		ctrl.registerUsername = "";
		ctrl.registerPassword = "";
		ctrl.registerPassword2 = "";



		// ==
		// == Methods
		// ==

		ctrl.doRegister = function (registerForm)
		{
			console.log("doRegister(" + registerForm.$valid + ")");

			// Submit when the form is valid
			if (registerForm.$valid)
			{
				UserService
					.register(ctrl.registerUsername, ctrl.registerPassword, ctrl.registerName, ctrl.registerSurnames)
					.then(registerSuccess, registerFailed);
			}
			else
			{
				registerForm.$setSubmitted();
			}
		}



		// ==
		// == Auxiliar
		// ==

		function registerSuccess (user)
		{
			$state.go("private.me");
		}

		function registerFailed (error)
		{
			console.error("Register failed:");
			console.error(error);
		}
	});

})();
