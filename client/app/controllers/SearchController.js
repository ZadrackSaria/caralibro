(function () {

	var app = angular.module("caralibro");

	app.controller("SearchController", function ($stateParams, UserService)
	{
		console.log("SearchController");
		var ctrl = this;


		// ==
		// == Properties
		// ==

		ctrl.query = $stateParams.query;
		ctrl.userList = [];



		// ==
		// == Initialize
		// ==

		UserService.search(ctrl.query).then(onGetSearch, onError);



		// ==
		// == Auxiliar
		// ==

		function onGetSearch (userList)
		{
			console.log("> onGetSearch:");
			console.log(userList);

			ctrl.userList = userList;
		}

		function onError (error)
		{
			console.error("> onError:");
			console.error(error);
		}

	});

})();
