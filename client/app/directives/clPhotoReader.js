(function () {

	var app = angular.module("caralibro");

	app.directive("clPhotoReader", function ()
	{
		return {
			scope: { clPhotoReader : "&" },
			transclude: true,
			link: function (scope, element, attrs)
			{
				var transcluded = element.find("div").first();
				var input = element.find("input").first();

				transcluded.on("click", function ()
				{
					console.log("click on transcluded");
					input.click();
				});

				input.bind("change", function (changeEvent)
				{
					scope.$apply(function ()
					{
						scope.clPhotoReader()(changeEvent.target.files[0]);
					});
				});
			},
			templateUrl: "app/directives/cl-photo-reader.html"
		}
	});

})();
