(function () {

	var app = angular.module("caralibro");

	app.directive("clPosts", function ()
	{
		console.log("Using clPosts directive");

		return {
			restrict: "EA",
			templateUrl: "app/directives/cl-posts.html",
			scope: {
				postList: "=",
				hasMorePosts: "=",
				loadMore: "&"
			}
		}
	});

})();
