(function () {

	var app = angular.module("caralibro");

	app.factory("PostService", function ($q, PAGE_TAM)
	{
		console.log("PostService");
		var service = {};
		var Post = Parse.Object.extend("Post");


		// ==
		// == Getters
		// ==

		/**
		 * Get latest posts of this social network.
		 * @param {number} page Page to obtain.
		 * @return {Promise}
		 */
		service.getAll = function (page)
		{
			console.log("PostService::getAll()");

			page = page || 0;

			var defer = $q.defer();

			var query = new Parse.Query(Post);
			query.descending("createdAt");
			query.include("author");

			// Pagination
			query.skip(page * PAGE_TAM);
			query.limit(PAGE_TAM);

			query.find(
				{
					success: function (postList)
					{
						defer.resolve(postList);
					},
					error: function (error)
					{
						defer.reject(error);
					}
				});

			return defer.promise;
		}

		/**
		 * Get all posts of an user.
		 * @param userId User ID.
		 * @return {Promise}
		 */
		service.getPostsOfUser = function (userId, page)
		{
			console.log("PostService::getPostsOfUser()");

			page = page || 0;

			var defer = $q.defer();

			var query = new Parse.Query(Post);
			query.descending("createdAt");
			query.include("author");

			// Get by author
			var author = new Parse.User;
			author.id = userId;
			query.equalTo("author", author);

			// Pagination
			query.skip(page * PAGE_TAM);
			query.limit(PAGE_TAM);

			query.find(
				{
					success: function (postList)
					{
						defer.resolve(postList);
					},
					error: function (error)
					{
						defer.reject(error);
					}
				});

			return defer.promise;
		}



		// ==
		// == Create
		// ==

		/**
		 * Creates a new post to the current logged user.
		 * @param {string} message The message of the post.
		 * @param {File} photoFile Photo for the post (optional).
		 */
		service.create = function (message, photoFile)
		{
			console.log("PostService::create()");

			var defer = $q.defer();


			var post = new Post();

			post.set("message", message);
			post.set("author", Parse.User.current());

			// Without photo, save directly the post
			if (!photoFile) savePost();

			// Save photo, later the post
			else
			{
				var parseFile = new Parse.File(photoFile.name, photoFile);
				parseFile.save().then(
					function ()
					{
						// The file has been saved. Set on post
						post.set("photo", parseFile);
						savePost();
					},
					function (error)
					{
						defer.reject(error);
					}
				);
			}

			function savePost ()
			{
				post.save(null,
					{
						success: function (postCreated)
						{
							defer.resolve(postCreated);
						},
						error: function (object, error)
						{
							defer.reject(error);
						}
					});
			}

			return defer.promise;
		}

		return service;
	});

})();
