(function () {

	var app = angular.module("caralibro");

	app.factory("UserService", function ($q)
	{
		console.log("UserService");
		var service = {};


		// ==
		// == Session
		// ==

		/**
		 * Registers a new user.
		 * @param {string} username Username for access.
		 * @param {string} password Password for access.
		 * @param {string} name Name of the user.
		 * @param {string} surnames Surnames of the user.
		 * @return {Promise}
		 */
		service.register = function (username, password, name, surnames)
		{
			console.log("UserService::register()");

			var defer = $q.defer();

			var user = new Parse.User();
			user.set("username", username);
			user.set("password", password);
			user.set("name", name);
			user.set("surnames", surnames);

			user.signUp(null,
				{
					success: function (user)
					{
						defer.resolve(user);
					},
					error: function (user, error)
					{
						defer.reject(error);
					}
				});

			return defer.promise;
		}

		/**
		 * Logs in an user.
		 * @param {string} username The username to login.
		 * @param {string} password The password.
		 * @return {Promise}
		 */
		service.login = function (username, password)
		{
			console.log("UserService::login()");

			var defer = $q.defer();

			Parse.User.logIn(username, password,
				{
					success: function (user)
					{
						defer.resolve(user);
					},
					error: function (user, error)
					{
						defer.reject(error);
					}
				});

			return defer.promise;
		}

		/**
		 * Logs out the current user.
		 */
		service.logout = function ()
		{
			console.log("UserService::logout()");

			Parse.User.logOut();
		};

		/**
		 * Gets the current logged user.
		 * @returns {Parse.User} The logged user, null if not exists.
		 */
		service.getCurrent = function ()
		{
			console.log("UserService::getCurrent()");

			return Parse.User.current();
		};



		// ==
		// == Getters
		// ==

		/**
		 * Gets an user.
		 * @param userId The user ID of the user to obtain.
		 * @return {Promise}
		 */
		service.getUser = function (userId)
		{
			console.log("UserService::getUser()");

			var defer = $q.defer();

			var query = new Parse.Query(Parse.User);
			query.get(userId,
				{
					success: function (user)
					{
						defer.resolve(user);
					},
					error: function (object, error)
					{
						defer.reject(error);
					}
				});

			return defer.promise;
		}

		/**
		 * Searchs an user that name/surnames starts with the passed parameter.
		 * @param {string} strSearch Name or surname to search.
		 * @return {Promise}
		 */
		service.search = function (strSearch)
		{
			console.log("UserService::search(" + strSearch + ")");

			var defer = $q.defer();

			var queryName = new Parse.Query(Parse.User);
			queryName.startsWith("name", strSearch);

			var querySurnames = new Parse.Query(Parse.User);
			querySurnames.startsWith("surnames", strSearch);

			var query = new Parse.Query.or(queryName, querySurnames);
			query.find(
				{
					success: function (userList)
					{
						defer.resolve(userList);
					},
					error: function (error)
					{
						defer.reject(error);
					}
				});

			return defer.promise;
		}

		/**
		 * Searchs an user that username is equal to the passed parameter.
		 * @param {string} username Username to search.
		 * @return {Promise} Resolves when no exists another user with this username; otherwise rejects.
		 */
		service.checkIfExistsUsername = function (username)
		{
			console.log("UserService::checkIfExistsUsername(" + username + ")");

			var defer = $q.defer();

			var query = new Parse.Query(Parse.User);
			query.equalTo("username", username);

			query.find(
				{
					success: function (userList)
					{
						console.log("UserService::checkIfExistsUsername(" + username + ") -> " + userList.length);

						if (userList.length == 0) defer.resolve();
						else defer.reject();
					},
					error: function (error)
					{
						defer.reject(error);
					}
				});

			return defer.promise;
		}



		// ==
		// == Modify
		// ==

		/**
		 * Updates the info of the current logged user.
		 * @param {string} newAbout The new "About me" info.
		 * @return {Promise}
		 */
		service.updateUserInfo = function (newAbout)
		{
			console.log("UserService::updateUserInfo()");

			var defer = $q.defer();
			var current = service.getCurrent();

			current.set("about", newAbout);

			current.save(null,
				{
					success: function (user)
					{
						defer.resolve(user);
					},
					error: function (object, error)
					{
						defer.reject(error);
					}
				});

			return defer.promise;
		}

		/**
		 * Updates the profile photo of the current logged user.
		 * @param {File} newPhotoFile The new photo.
		 * @return {Promise}
		 */
		service.updateUserPhoto = function (newPhotoFile)
		{
			console.log("UserService::updateUserPhoto()");

			var defer = $q.defer();
			var current = service.getCurrent();
			var parseFile = new Parse.File(newPhotoFile.name, newPhotoFile);

			// Save file on Parse
			parseFile.save().then(
				function ()
				{
					// The file has been saved. Set on user photo profile
					current.set("photo", parseFile);
					current.save(null,
						{
							success: function (user)
							{
								defer.resolve(user);
							},
							error: function (object, error)
							{
								defer.reject(error);
							}
						});
				},
				function (error)
				{
					defer.reject(error);
				}
			);

			return defer.promise;
		}

		return service;
	});

})();
