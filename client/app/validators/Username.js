(function () {

	var app = angular.module("caralibro");

	app.directive("usernameExists", function ($q, UserService)
	{
		return {
			require: "ngModel",
			link: function (scope, elm, attrs, ctrl)
			{
				ctrl.$asyncValidators.usernameExists = function (modelValue, viewValue)
				{
					return UserService.checkIfExistsUsername(modelValue);
				}
			}
		}
	});

})();
